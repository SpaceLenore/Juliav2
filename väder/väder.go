package väder

import (
	"fmt"
	owm "github.com/briandowns/openweathermap"
)

func GetWeatherByName(location string) (string, error) {
	w, err := owm.NewCurrent("C", "se")
	if err != nil {
		return "", err
	}

	err = w.CurrentByName(location)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("Vädret i %s: %s, %.1f °C, %.1f m/s, %d%% luftfuktighet, %d%% molntäcke, %.1f hPa", w.Name, w.Weather[0].Description, w.Main.Temp, w.Wind.Speed, w.Main.Humidity, w.Clouds.All, w.Main.Pressure), nil
}
