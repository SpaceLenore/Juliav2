package bestäm

import (
	"math/rand"
	"strings"
	"time"
)

func Bestäm(in []string) string {
	for i := range in {
		if strings.EqualFold(in[i], "eller") {
			in[i] = "eller"
		}
	}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	s := strings.Split(strings.Join(in, " "), "eller")
	return strings.TrimSpace(s[r.Intn(len(s))])
}
