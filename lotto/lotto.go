package lotto

import (
	"fmt"
	"math/rand"
	"time"
)

func GetLine() string {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	return fmt.Sprintf("%d", r.Perm(35)[:7])
}
