package main

import (
	"golang.org/x/net/proxy"
	"html"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
)

func getTitle(input string) (string, error) {
	proxyurl, err := url.Parse("socks5://127.0.0.1:9050")
	if err != nil {
		return "", err
	}
	proxydialer, err := proxy.FromURL(proxyurl, proxy.Direct)
	if err != nil {
		return "", err
	}
	transport := &http.Transport{Dial: proxydialer.Dial}
	client := &http.Client{Transport: transport}

	req, err := http.NewRequest("GET", input, nil)
	if err != nil {
		return "", err
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3100.0 Safari/537.36")
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 1<<18))
	if err != nil {
		return "", err
	}

	re := regexp.MustCompile("<title>(.{1,128})<\\/title>")
	titleMatch := re.FindStringSubmatch(string(body))
	if len(titleMatch) > 0 {
		return html.UnescapeString(titleMatch[1]), nil
	}
	return "", nil

}
